<?php
/**
 * 
 * PHP version >= 7.0
 * 
 * @category Console_Command
 * @package App\Console\Commands
 */

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Symfony\Component\Console\Output\ConsoleOutput;

use App\Agl\Api\ArrayFilterInterface;
use App\Agl\AnimalService;
use App\Agl\WebServiceHandler;
use App\Agl\ArrayFilter;

/**
 * Class GetAnimalCommand
 * 
 * @category Console_Command
 * @package App\Console\Commands;
 */

class GetAnimalsCommand extends \Illuminate\Console\Command
{
    use \App\Agl\JsonUtil;
    /**
     * Command Name
     * 
     * @var string
     */
    protected $name = 'agl:get-cats';
    
    protected $animalService;


    /**
     * Description
     * 
     * @var string
     */
    protected $description = "Get Cats from Cat Service";
    
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Get Name of the command
     * 
     * @return string
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * Execute command
     * 
     * @return mixed
     */
    public function handle() {
        $animals = $this->getAnimals();
        dd($animals);
    }
    
    /**
     * 
     * @return array
     */
    private function getAnimals() : array {
        $this->animalService = new AnimalService(
                new WebServiceHandler(),
                new ArrayFilter()
                );
        $response = $this->animalService->getSortedAnimals(env("ANIMAL_OWNERS"));
        return $response;
    }
}