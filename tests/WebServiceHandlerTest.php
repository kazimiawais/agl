<?php

use App\Agl\WebServiceHandler;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\HandlerStack;

class WebSerivceHandlerTest extends TestCase
{
    
    /**
     * @var GuzzleHttp\Handler\MockHandler;
     */
    protected $mock;
    
    /**
     * @var App\Agl\WebServiceHandler 
     */
    protected $client;
    
    /**
     * setUp
     */
    public function setUp() : void {
        parent::setUp();
        
        $this->mock = new MockHandler([
            new Response(200, ['X-Foo' => 'Bar'], '{"1": "Hello", "2" : "world" }'),
            new Response(202, ['Content-Length' => 0]),
        ]);
        
        $handlerStack = HandlerStack::create($this->mock);

        $this->client = new WebServiceHandler([
            'handler' => $handlerStack,
        ]);
    }
    
    /**
     * @test
     */
    public function testOKWebServiceHandler() {
        
        // Test OK Response
        $response = $this->client->call('GET', '/');
        $this->assertEquals(200, $response->getStatusCode());
        
        $body = $response->getBody();
        $this->assertJson($body);
        
        $response = $this->client->call('GET', '/');
        $this->assertEquals(202, $response->getStatusCode());
    }
    
    public function testMockReset201() {
        $this->mock->reset();
        $this->mock->append(new Response(201));
        
        $response = $this->client->request('GET', '/');
        $this->assertEquals(201, $response->getStatusCode());
    }
    
    /**
     * tearDown
     * 
     * @void
     */
    public function tearDown() : void {
        parent::tearDown();
        
        $this->mock = null;
        $this->client = null;
    }

}