<?php
namespace App\Agl\Api;

/**
 * 
 * Defines a contract for all classes which are
 * interested in using array filtering service
 */
interface ArrayFilterInterface
{
    public function filterCatsByOwnerGender() : array;
}