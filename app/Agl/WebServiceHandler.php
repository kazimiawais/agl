<?php
namespace App\Agl;

use App\Agl\Api\CallableInterface;
use GuzzleHttp\Exception\ClientException;

class WebServiceHandler extends \GuzzleHttp\Client implements CallableInterface
{
    /**
     * 
     * @param array $config
     */
    public function __construct(array $config = array()) {
        parent::__construct($config);
    }
    
    /**
     * 
     * @param \GuzzleHttp\ClientInterface $client
     * @param string $uri
     * @return GuzzleHttp\Psr7\Response
     * @throws \Exception
     */
    public function call(string $method, string $uri) {
        
        try {
            
            $promise = $this->requestAsync($method, $uri);
            $response = $promise->wait();
            
            return $response;
        
        } catch (Exception $ex) {
            throw $ex;
        }
        
    }
}