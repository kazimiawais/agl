<?php
namespace App\Agl\Api;

/**
 * 
 * Defines a contract for all those classes
 * which are interested calling Web Api endpoints
 */
interface CallableInterface 
{
    public function call(string $method, string $uri);
}