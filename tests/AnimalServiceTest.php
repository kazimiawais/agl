<?php
use App\Agl\AnimalService;
use App\Agl\WebServiceHandler;
use App\Agl\ArrayFilter;

class AnimalServiceTest extends TestCase 
{
    
    protected $animalService;
    
    public function setUp() : void {
        parent::setUp();
        $this->animalService = new AnimalService(
                new WebServiceHandler(),
                new ArrayFilter()
                );
    }
    
    /**
     * @test
     */
    public function testAnimalServiceIsNotNull() : void {
        $this->assertNotNull($this->animalService);
    }
    
    public function tearDown() : void {
        parent::tearDown();
    }
}

