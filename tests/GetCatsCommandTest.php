<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\Console\Application as ConsoleApplication;
use Illuminate\Support\Facades\Artisan;

class GetAnimalsCommandTest extends TestCase
{
    /**
     *
     * @var \Illuminate\Console\Command 
     */
    protected $command;
    
    
    public function setUp() : void {
        parent::setUp();
        
        $application = new ConsoleApplication();
        
        $loadedCommand = $this->app->make(App\Console\Commands\GetAnimalsCommand::class);
        $loadedCommand->setLaravel(app());
        $application->add($loadedCommand);
        
        $this->command = $application->find('agl:get-cats');
        
    }
    
    /**
     * @test
     *
     * @return void
     */
    public function testCommandExistsToGetCatsData() : void
    {
        $this->assertEquals('agl:get-cats', $this->command->getName());
    }
}
