<?php
use App\Agl\JsonUtil;

class JsonUtilTest extends TestCase 
{
    /**
     *
     * @var JsonUtil
     */
    protected $trait;
    
    public function setUp() : void {
        parent::setUp();
        $this->trait = $this->getMockForTrait(App\Agl\JsonUtil::class);
        
    }
    
    public function testInvalidJsonToArray() {
        $json = "abadje;rer";
        
        $response = $this->trait->getArray($json);
        
        $this->assertFalse($response);
        
        
    }
    
    public function testConvertingJsonToArray() {
        $json = '{"name": "John", "age": 31, "city": "New York"}';
        $assoArray = array();
        
        $assoArray = $this->trait->getArray($json);
        
        $this->assertGreaterThan(0, count($assoArray));
    }


    public function tearDown() : void {
        parent::tearDown();
    }
}
