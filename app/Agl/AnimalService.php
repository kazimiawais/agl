<?php
namespace App\Agl;

use App\Agl\Api\CallableInterface;
use App\Agl\Api\ArrayFilterInterface;
use App\Agl\ArrayFilter;
use App\Agl\WebServiceHandler;
use App\Agl\JsonUtil;


class AnimalService 
{
    use JsonUtil;
    
    /**
     *
     * @var App\Agl\Api\CallableInterface 
     */
    protected $callable;
    
    /**
     *
     * @var App\Agl\Api\ArrayFilterInterface
     */
    protected $arrayFilter;
    
    /**
     *
     * @var string
     */
    protected $url;

    /**
     * 
     * @param App\Agl\Api\CallableInterface $callable
     * @param App\Agl\Api\ArrayFilterInterface $arrayFilter
     */
    public function __construct(
        CallableInterface $callable,
        ArrayFilterInterface $arrayFilter
            ) {
        $this->callable = $callable;
        $this->arrayFilter = $arrayFilter;
    }
    
    /**
     * Get Sorted Animals list by owner's gender
     * 
     * @param string $url
     * @return array
     */
    public function getSortedAnimals($url = "") : array {
        if("" === $url) return FALSE;
        
        $this->url = $url;

        // Get Json
        $json = $this->callable
                ->call("GET", $this->url)
                ->getBody();
        
        $jsonToArray = $this->getArray($json);
        
        $output = $this->arrayFilter->filterCatsByOwnerGender($jsonToArray);
        
        return $output;
    } 
}