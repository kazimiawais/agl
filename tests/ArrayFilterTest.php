<?php
use App\Agl\Api\ArrayFilterInterface;
use App\Agl\ArrayFilter;
use App\Agl\JsonUtil;

class ArrayFilterTest extends TestCase 
{
    /**
     * JsonUtil Trait
     * @var JsonUtil
     */
    protected $jsonUtil;
    
    public function setUp() : void {
        parent::setUp();
        $this->jsonUtil = $this->getMockForTrait(App\Agl\JsonUtil::class);
    }
    
    /**
     * @test
     */
    public function testEmptyArray() {
        $emptyArray = array();
        $arrayFilter = new ArrayFilter($emptyArray);
        $response = $arrayFilter->filterCatsByOwnerGender();
        
        $this->assertEmpty($response);
    }
    
    /**
     * @test
     */
    public function testFilteringArray() {
        $json = file_get_contents(__DIR__ . "/fixtures/owners.json");
        
        $array = $this->jsonUtil->getArray($json);

        $arrayFilterService = new ArrayFilter();
        
        $animals = $arrayFilterService->filterCatsByOwnerGender($array);

        $this->assertGreaterThan(0, $animals);
        
        $this->assertArrayHasKey("Male", $animals);
        
        $this->assertArrayHasKey("Female", $animals);
        
        $this->assertGreaterThan(0, count($animals["Male"]));
        
        $this->assertGreaterThan(0, count($animals["Female"]));
                
    }
    
    public function testGettingAnimalOwnersFromEnv() {
        $url = env("ANIMAL_OWNERS");
        
        $validation = filter_var($url, FILTER_VALIDATE_URL);
        
        $this->assertNotFalse($validation);
    }

    

    public function tearDown() : void {
        parent::tearDown();
    }
}