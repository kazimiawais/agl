<?php
namespace App\Agl;

/**
 * Defines all Json related PHP utility functions in one place
 */
trait JsonUtil
{
    /**
     * 
     * @param string $json
     * @return array|bool
     */
    public function getArray(string $json = "") {
        if(!$this->json_validator($json)) {
            return false;
        }
        
        return json_decode($json, TRUE);
    }
    
    private function json_validator($data=NULL) : bool {
        if (!empty($data)) {
                @json_decode($data);
                return (json_last_error() === JSON_ERROR_NONE);
        }
        return false;
    }
}