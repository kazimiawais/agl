<?php
namespace App\Agl;

use App\Agl\Api\ArrayFilterInterface;

class ArrayFilter implements ArrayFilterInterface
{
    /**
     * @var array $catOwners
     */
    protected $catOwners;
    
    /**
     *
     * @var array
     */
    protected $results;
    
    
    public function __construct() {
        
    }

    /**
     * Gets an array of cat owners and returns
     * an associative array for all cats classified under
     * their owner's gender.
     * 
     * @param array $catOwners
     * @return array
     */
    public function filterCatsByOwnerGender($catOwners = array()) : array {
        if(0 == count($catOwners)) {
            return array();
        }
        $this->catOwners = $catOwners;
        $this->results = [
            "Male" => array(),
            "Female" => array(),
        ];
        
        foreach($this->catOwners as $owner) {
            $this->getGetPetsUnderOwnerGender($owner);
        }
        
        sort($this->results["Male"]);
        sort($this->results["Female"]);
        
        return $this->results;
    }
    
    /**
     * Receive a single owner and separates its pets
     * under gender key in associative array
     * 
     * @param array $owner
     * @return void
     */
    private function getGetPetsUnderOwnerGender($owner = array()) : void {
        if(
            array_key_exists("gender", $owner)
            && (in_array($owner["gender"], array("Male", "Female")))
          ) {
            
            $gender = $owner['gender'];

            if($owner['pets'] !== null && 0 < count($owner['pets'])) {
                array_map(function($pet) use ($gender) {
                    array_push($this->results[$gender], $pet['name']);
                }, $owner['pets']);
            }
        }
    }
}